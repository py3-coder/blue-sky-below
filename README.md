# Blue Sky Below

## Goal

Forecasting Sensor Measurements in Smart Air Aquality Monitoring System

## Background:

Air quality has a significant impact on the overall well-being of humans and society across the globe. The rewards of good air quality are numerous, including substantial health, environmental, and economic benefits.  However, as a result of increasing urbanisation and industrialisation, air quality in major cities around the world is becoming a source of concern. Several nations have made efforts to implement smart city initiatives, in which sensors play a vital role in informing both governing authorities and the general public about real-time air quality levels via mobile or web-based apps.  Traditional sensor monitoring can be made smarter through the adoption of state-of-the-art machine learning algorithms, which will allow for an improvement in the current capabilities of air quality monitoring. In this context, the sub-theme 2 of the hackathon seeks to discover new innovative solutions for developing smart air quality monitoring systems by integrating sensor technology with machine learning algorithms.

**Temporal forecasting of temperature and Carbon Monoxide (CO) sensor data one day ahead: It can assist the general public and government officials in anticipating trends early in order to make timely decisions and take preventative actions**.


## Dataset 
> https://www.kaggle.com/fedesoriano/air-quality-data-set?select=AirQuality.csv

A number of factors in the air can have an impact on its quality. Multiple sensors monitoring various parameters are used in air quality monitoring sensing systems, which are available as a whole suite. The role of temperature  and carbon monoxide in air quality is vital.


## DataSet - Details :

- In total, the dataset contains 15 attributes. In this , we are restricting to use only 4 attributes. They are:

Attribute 00: Date (DD/MM/YYYY)

Attribute 01: Time (HH.MM.SS)

Attribute 02: CO (mg/m3)

Attribute 12: Temperature (°C)

## Initial 
- Training Data Period: 7 days from 11/03/2004 00.00.00 to 17/03/2004 23.00.00
- Testing Data Period: 7 days from 18/03/2004 00.00.00 to 24/03/2004 23.00.00

- Each day of the training and test data period have 24 data points starting from 00.00.00 to 23.00.00.


## Procedure:

> a) Initially, train your machine learning model using 7 days data from 11/03/2004 00.00.00 to 17/03/2004 23.00.00.

> b) Perform temporal forecasting (one-day ahead forecasting) for the 8th day using 7 days of data. Compare the forecast values with the real sensor data and perform performance evaluation using the metrics Mean Absolute Percentage Error (MAPE). Use your real sensor data for the 8th day as the true value while computing the performance metric for the 8th

> c) Perform the temporal forecasting for the 9th day by updating the training database from the 8th day sensor measurements. Compute the forecasting performance metric for the 9th

> d) Perform the temporal forecasting for the 10th day by updating the training database from the 9th day sensor measurements. Compute the forecasting performance metric for the 10th

> e) Perform the temporal forecasting for the 11th day by updating the training database from the 10th day sensor measurements. Compute the forecasting performance metric for the 11th

> f) Perform the temporal forecasting for the 12th day by updating the training database from the 11th day sensor measurements. Compute the forecasting performance metric for the 12th

> g) Perform the temporal forecasting for the 13th day by updating the training database from the 12th day sensor measurements. Compute the forecasting performance metric for the 13th

> h) Perform the temporal forecasting for the 14th day by updating the training database from the 13th day sensor measurements. Compute the forecasting performance metric for the 14th

## References:

Similar Work: K. Thiyagarajan, S. Kodagoda, L. Van Nguyen and R. Ranasinghe, "Sensor Failure Detection and Faulty Data Accommodation Approach for Instrumented Wastewater Infrastructures," in IEEE Access, vol. 6, pp. 56562-56574, 2018, doi: 10.1109/ACCESS.2018.2872506.


## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment


## License


## Project status


